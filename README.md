options :
```
- n_phi -> number_of_philosophers
- t_die -> time_to_die
- t_eat -> time_to_eat
- t_slp -> time_to_sleep
- n_eat -> [number_of_times_each_philosopher_must_eat]
```

**thread :**

- parts of a process that performs code simultaneously
- thread are light-weight-process (LWP) that happens inside a process (observe with command `ps -eLf`)
- a process can be single-threaded or multi-threaded
- different process have a different PID and different LWP, different thread in the same process have the same PID and different LWP
- thread vs process :

  *process :*

  - process is isolated, it doesn't share memory with any other process
  - process is less efficient in communication, it takes more times to create or terminate or switch
  - process speed is not impacted by speed of other process (untill it reach the limit of cpu)

  *thread :*

  - thread are not isolated, they share memory with the other threads
  - thread is more efficient in communication, it takes less times to create or terminate or switch
  - thread can become slow if the process does many concurrent tasks

- ressources :
  - https://www.baeldung.com/linux/process-vs-thread

**mutex :**

- mutual exclusion
- to prevent a part of the code to be performed simultaneously by threads
- the section of code wraped by a mutex can be access by only one thread at a time
- the section starts by unlocking a mutex, and end by locking it

**external function :**

- `memset`      : fill memory with a constant byte
- `printf`      : format and print data
- `malloc`      : allocate dynamic memory
- `free`        : free dynamic memory
- `write`       : write to a file descriptor
- `usleep`      : suspend execution for microseconds intervals
- `gettimeofday`: get time
- `pthread_create`       : create a new thread
- `pthread_join`         : wait for a thread to finish, 
- `pthread_detach`       : when a thread is detached it cannot be join, you cannot wait for it to teminate, it will release its ressources on its own when it has finish
- `pthread_mutex_init`   : initialize a mutex
- `pthread_mutex_destroy`: destroy a mutex
- `pthread_mutex_unlock` : unlock a mutex
- `pthread_mutex_lock`   : lock a mutex

