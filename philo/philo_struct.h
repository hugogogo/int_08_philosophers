/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_struct.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hulamy <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/26 15:27:23 by hulamy            #+#    #+#             */
/*   Updated: 2022/01/31 10:20:22 by hulamy           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_STRUCT_H
# define PHILO_STRUCT_H

typedef pthread_mutex_t	t_mtx;

typedef struct s_time
{
	long int	ts;
	long int	tu;
}		t_time;

typedef struct s_params
{
	int			n_phi;
	int			t_die;
	int			t_eat;
	int			t_slp;
	int			n_eat;
}		t_params;

typedef struct s_global
{
	int			stop;
	int			satiated_count;
	t_time		t_start;
	t_mtx		m_print;
	t_mtx		m_init_time;
	t_mtx		m_stop;
}		t_global;

typedef struct s_philo
{
	t_params		*params;
	t_global		*global;
	int				p_nbr;
	t_mtx			m_fork;
	t_mtx			m_time;
	t_mtx			m_eat;
	t_time			t_last_meal;
	int				eat_count;
	struct s_philo	*next;
}					t_philo;

#endif
