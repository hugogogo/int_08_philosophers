/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hulamy <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/26 15:30:19 by hulamy            #+#    #+#             */
/*   Updated: 2022/01/26 18:04:08 by hulamy           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	ft_isdigit_2d_arr(char **str)
{
	int	i;
	int	j;

	i = 0;
	while (str[i])
	{
		j = 0;
		while (str[i][j])
		{
			if (str[i][j] < '0' || str[i][j] > '9')
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}

size_t	ft_strlen(char *str)
{
	size_t	i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

int	ft_strncmp(char *s1, char *s2, size_t n)
{
	size_t	i;
	int		res;

	i = 0;
	res = 0;
	while (s1[i] && s1[i] == s2[i] && i < n - 1)
		i++;
	if (n != 0)
		res = (unsigned char)s1[i] - (unsigned char)s2[i];
	return (res);
}

int	ft_int_overflow(char *str)
{
	size_t	len;
	char	*int_xtrem;

	int_xtrem = "2147483647";
	if (str[0] == '-')
		int_xtrem = "2147483648";
	if (str[0] == '+' || str[0] == '-')
		str++;
	len = ft_strlen(str);
	if (len < 10)
		return (0);
	else if (len > 10 || ft_strncmp(str, int_xtrem, len) > 0)
		return (1);
	return (0);
}

int	ft_atoi(const char *str)
{
	int	i;
	int	result;
	int	is_negative;

	is_negative = 0;
	result = 0;
	i = 0;
	while (str[i] == ' ' || (str[i] >= 9 && str[i] <= 13))
		i++;
	if (str[i] == '+')
		i++;
	else if (str[i] == '-')
	{
		is_negative = 1;
		i++;
	}
	while (str[i] >= '0' && str[i] <= '9')
	{
		result = (result * 10) + (str[i] - '0');
		i++;
	}
	if (is_negative)
		result = result * -1;
	return (result);
}
