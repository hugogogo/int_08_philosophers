/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hulamy <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/26 15:30:40 by hulamy            #+#    #+#             */
/*   Updated: 2022/02/02 00:14:59 by hulamy           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	*return_free(void *e1, void *e2, void *e3)
{
	if (e1)
		free(e1);
	if (e2)
		free(e2);
	if (e3)
		free(e3);
	return (NULL);
}

void	free_philo(t_philo *philo, int n)
{
	t_philo	*tmp;
	int		i;

	if (philo && philo->params)
		free(philo->params);
	if (philo && philo->global)
		free(philo->global);
	i = 1;
	while (i <= n && philo)
	{
		if (i < n)
			tmp = philo->next;
		free(philo);
		if (i < n)
			philo = tmp;
		i++;
	}
}

int	main(int ac, char **av)
{
	pthread_t	*id;
	t_philo		*philo;
	int			i;

	philo = init(ac, av, &id);
	if (philo == NULL)
		return (0);
	launch(philo, id);
	i = 0;
	while (i < philo->params->n_phi)
	{
		pthread_join(id[i], NULL);
		i++;
	}
	free_philo(philo, philo->params->n_phi);
	free(id);
	return (0);
}
