/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_proto.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hulamy <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/26 15:27:13 by hulamy            #+#    #+#             */
/*   Updated: 2022/02/02 00:15:43 by hulamy           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_PROTO_H
# define PHILO_PROTO_H

// main.c
void	*return_free(void *e1, void *e2, void *e3);

// init.c
t_philo	*init(int ac, char **av, pthread_t **id);

// launch.c
void	launch(t_philo *philo, pthread_t *id);

// exec.c
void	*philo_exec(void *arg);

// generic.c
void	init_time(t_philo *philo);
void	update_time(t_philo *philo);
int		diff_time(t_philo *philo, struct timeval *new);
int		print_message(t_philo *philo, char *clr, char *msg);

// utils.c
int		ft_isdigit_2d_arr(char **str);
size_t	ft_strlen(char *str);
int		ft_strncmp(char *s1, char *s2, size_t n);
int		ft_int_overflow(char *str);
int		ft_atoi(const char *str);

#endif
